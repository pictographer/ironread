# IrOnRead

If you use a Samsung TV as a monitor, this Arduino sketch enables you to toggle the TV power from the keyboard. The Arduino sketch sends the IR code to toggle the TV power when it either
    - receives a byte via USB Serial, or
    - senses that the microcontroller board has been touched near pin 3.

This sketch assumes a Teensy LC with an IR LED and resistor connecting pin 16 to Ground. Pin 16 was selected based on https://www.pjrc.com/teensy/td_libs_IRremote.html

Thanks to Ekus (https://gist.github.com/Ekus) for the Samsung example:
https://gist.github.com/Ekus/92842e60e8a3f53f38d3

For Ubuntu 20.04, you can define a keyboard shortcut to run a shell script to echo a byte to /dev/ttyACM0. Settings > Keyboard Shortcuts > +
