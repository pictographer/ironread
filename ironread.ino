// Copyright © 2022 John D. Corbett <corbett@pictographer.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without restriction,
// including without limitation the rights to use, copy, modify, merge, publish, distribute,
// sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


// If you use a Samsung TV as a monitor, this Arduino sketch enables you to toggle the TV
// power from the keyboard. The Arduino sketch sends the IR code to toggle the TV power
// when it either
//    - receives a byte via USB Serial, or
//    - senses that the microcontroller board has been touched near pin 3.
//
// This sketch assumes a Teensy LC with an IR LED and resistor connecting pin 16 to Ground.
// Pin 16 was selected based on https://www.pjrc.com/teensy/td_libs_IRremote.html
//
// Thanks to Ekus (https://gist.github.com/Ekus) for the Samsung example:
// https://gist.github.com/Ekus/92842e60e8a3f53f38d3
//
// For Ubuntu 20.04, you can define a keyboard shortcut to run a shell script to echo a byte
// to /dev/ttyACM0. Settings > Keyboard Shortcuts > +

#define IR_SEND_PIN 16
#include <IRremote.hpp>

template<typename T>
constexpr auto sizeof_array(const T& iarray) {
  return (sizeof(iarray) / sizeof(iarray[0]));
}

const uint16_t togglePower[] = {
  4600, 4350, 700, 1550, 650,
  1550, 650,
  1600, 650,
  450, 650,
  450, 650,
  450, 650,
  450, 700, 400, 700, 1550, 650,
  1550, 650,
  1600, 650,
  450, 650,
  450, 650,
  450, 700,
  450, 650,
  450, 650,
  450, 650,
  1550, 700,
  450, 650,
  450, 650,
  450, 650,
  450, 650,
  450, 700, 400, 650,
  1600, 650, 450, 650,
  1550, 650,
  1600, 650,
  1550, 650,
  1550, 700, 1550, 650,
  1550, 650
};

const uint16_t toggleSource[] = {
  4600, 4350, 700, 1550, 650,
  1550, 700,
  1500, 700,
  450, 650,
  450, 700, 400, 700, 400, 700, 400, 700,
  1550, 700,
  1500, 700,
  1550, 700, 400, 700, 400, 700, 400, 700, 400, 700, 400, 700,
  1550, 700, 400, 700,
  450, 650,
  450, 650,
  450, 700, 400, 700, 400, 700, 400, 700,
  450, 650,
  1550, 700,
  1500, 700,
  1550, 650,
  1550, 700,
  1500, 700,
  1550, 700,
  1500, 700
};

uint_fast8_t frequency_kHZ = 38;

// The Teensy LC has capacitive touch sensing. You may need to print
// values to find an appropriate threshold.
const int TOUCH_PIN = 3;
const int SOURCE_PIN = 23;
const int TOUCH_THRESHOLD = 450;

void setup() {
  Serial.begin(9600);
  IrSender.begin(IR_SEND_PIN, false, 0);
}

void loop() {
  auto in = Serial.read();

  switch (in) {
    case 's':
      {
        IrSender.sendRaw(togglePower, uint_fast8_t(sizeof_array(toggleSource)), frequency_kHZ);
        delay(250);
        break;
      }
    case 'a':
    case 'p':
      {
        IrSender.sendRaw(togglePower, uint_fast8_t(sizeof_array(togglePower)), frequency_kHZ);
        delay(250);
        break;
      }
    default: break;
  }

  if (touchRead(TOUCH_PIN) > TOUCH_THRESHOLD) {
    IrSender.sendRaw(togglePower, uint_fast8_t(sizeof_array(togglePower)), frequency_kHZ);
    delay(250);
  }

  if (touchRead(SOURCE_PIN) > TOUCH_THRESHOLD) {
    IrSender.sendRaw(toggleSource, uint_fast8_t(sizeof_array(toggleSource)), frequency_kHZ);
    delay(250);
  }
}
